# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/filters/votes_conditions_filter'

describe Gitlab::Triage::Filters::VotesConditionsFilter do
  let(:upvotes) { 5 }
  let(:downvotes) { 1 }

  let(:resource) do
    {
      upvotes: upvotes,
      downvotes: downvotes
    }
  end

  let(:condition) do
    {
      attribute: 'upvotes',
      condition: 'greater_than',
      threshold: 3
    }
  end

  subject { described_class.new(resource, condition) }

  it_behaves_like 'a filter'

  describe '#resource_value' do
    it 'has the correct value for upvotes attribute' do
      expect(subject.resource_value).to eq(upvotes)
    end

    it 'has the correct value for the downvotes attribute' do
      filter = described_class.new(resource, condition.merge(attribute: 'downvotes'))
      expect(filter.resource_value).to eq(downvotes)
    end
  end

  describe '#condition_value' do
    it 'has the correct value for comparison' do
      expect(subject.condition_value).to eq(3)
    end
  end

  describe '#calculate' do
    it 'calculates true given correct condition' do
      expect(subject.calculate).to be(true)
    end

    it 'calculate false given wrong condition' do
      filter = described_class.new(resource, condition.merge(condition: 'less_than'))
      expect(filter.calculate).to be(false)
    end
  end
end
