# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/action/delete'

describe Gitlab::Triage::Action::Delete do
  include_context 'with network context'

  let(:type) { 'branches' }
  let(:resources) do
    [
      { name: 'branch-name', protected: false, type: 'branches' }
    ]
  end

  let(:policy) do
    Gitlab::Triage::Policies::RulePolicy.new(
      type,
      {
        name: 'Test delete action',
        actions: actions_hash
      },
      Gitlab::Triage::PoliciesResources::RuleResources.new(resources),
      network
    )
  end

  let(:actions_hash) do
    {
      delete: true
    }
  end

  subject do
    described_class.new(
      policy: policy, network: network)
  end

  describe '#act' do
    it 'delete the resources' do
      stub_delete = stub_delete(
        "http://test.com/api/v4/projects/123/repository/branches/branch-name"
      )

      subject.act

      assert_requested(stub_delete)
    end

    context 'when there is no resources' do
      let(:resources) { [] }

      it 'does not delete anything' do
        expect(subject).not_to receive(:perform)

        subject.act
      end
    end
  end
end
