# frozen_string_literal: true

RSpec.shared_examples 'label command' do |command|
  let(:labels) do
    ['bug', 'feature proposal']
  end

  let(:label_exist) { true }
  let(:label_stub) { double(exist?: label_exist) }

  %i[project_id group_id].each do |resource_id_key|
    context "when resource has a #{resource_id_key}" do
      before do
        labels.each do |label|
          allow(Gitlab::Triage::Resource::Label).to receive(:new).with({ resource_id_key => 42, :name => label },
            { network: nil }).and_return(label_stub)
        end
      end

      subject { described_class.new(labels, resource: { resource_id_key => 42 }) }

      describe '#build_command' do
        it 'outputs the correct command' do
          expect(subject.build_command).to eq("/#{command} ~\"bug\" ~\"feature proposal\"")
        end

        context 'when no labels are present' do
          let(:labels) { [] }

          it 'outputs an empty string' do
            expect(subject.build_command).to eq("")
          end
        end

        context 'when label does not exist' do
          let(:label_exist) { false }

          it 'outputs an empty string' do
            expect { subject.build_command }.to raise_error(Gitlab::Triage::Resource::Label::LabelDoesntExistError)
          end
        end
      end
    end
  end
end
