# frozen_string_literal: true

require 'spec_helper'

describe 'select merge requests by reviewer_id' do
  include_context 'with integration context'

  let(:mr_with_reviewer) do
    mr.merge(title: 'With Reviewer')
  end

  let(:mr_without_reviewer) do
    mr.merge(title: 'Without Reviewer')
  end

  before do
    stub_api_query(reviewer_id, merge_request)
  end

  shared_examples 'matched rule' do
    it 'posts the correct comment on the merge request' do
      stub_post = stub_api_comment(merge_request[:iid], comment)

      perform(rule(reviewer_id, comment))

      assert_requested(stub_post)
    end
  end

  context 'with `any` reviewer_id' do
    let(:reviewer_id) { 'any' }
    let(:merge_request) { mr_with_reviewer }
    let(:comment) { 'Comment because MR has any reviewer.' }

    it_behaves_like 'matched rule'
  end

  context 'with `none` reviewer_id' do
    let(:reviewer_id) { 'none' }
    let(:merge_request) { mr_without_reviewer }
    let(:comment) { 'Comment because MR has no reviewer.' }

    it_behaves_like 'matched rule'
  end

  context 'with specific reviewer_id' do
    let(:reviewer_id) { 123 }
    let(:merge_request) { mr_with_reviewer }
    let(:comment) { 'Comment because MR has reviwer 123.' }

    it_behaves_like 'matched rule'
  end

  def stub_api_query(reviewer_id, merge_request)
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests",
      query: { per_page: 100, reviewer_id: reviewer_id },
      headers: { 'PRIVATE-TOKEN' => token }) do
      [merge_request]
    end
  end

  def stub_api_comment(iid, body)
    stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{iid}/notes",
      body: { body: body },
      headers: { 'PRIVATE-TOKEN' => token })
  end

  def rule(reviewer_id, comment)
    <<~YAML
      resource_rules:
        merge_requests:
          rules:
            - name: Rule name
              conditions:
                reviewer_id: #{reviewer_id}
              actions:
                comment: |
                  #{comment}
    YAML
  end
end
