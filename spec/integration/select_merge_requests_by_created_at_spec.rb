# frozen_string_literal: true

require 'spec_helper'

describe 'select merge_requests by created_at' do
  around do |example|
    travel_to(Time.utc(2020, 5, 15)) do
      example.run
    end
  end

  include_context 'with integration context'

  let!(:mr_from_3_months_ago) do
    mr.merge(created_at: 3.months.ago)
  end

  let!(:mr_from_1_month_ago) do
    mr.merge(created_at: 1.month.ago, iid: issue[:iid] * 2)
  end

  describe 'created_after' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests",
        query: { per_page: 100, created_after: Time.utc(2020, 3, 15).to_date },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [mr_from_1_month_ago]
      end
    end

    it 'comments on the merge request' do
      rule = <<~YAML
        resource_rules:
          merge_requests:
            rules:
              - name: Rule name
                conditions:
                  date:
                    attribute: created_at
                    condition: newer_than
                    interval_type: months
                    interval: 2
                actions:
                  comment: |
                    Comment because its created at date is newer than 2 months ago.
      YAML

      stub_post_mr_from_1_month_ago = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{mr_from_1_month_ago[:iid]}/notes",
        body: { body: 'Comment because its created at date is newer than 2 months ago.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_mr_from_1_month_ago)
    end
  end

  describe 'created_before' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests",
        query: { per_page: 100, created_before: Time.utc(2020, 3, 15).to_date },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [mr_from_3_months_ago]
      end
    end

    it 'comments on the merge request' do
      rule = <<~YAML
        resource_rules:
          merge_requests:
            rules:
              - name: Rule name
                conditions:
                  date:
                    attribute: created_at
                    condition: older_than
                    interval_type: months
                    interval: 2
                actions:
                  comment: |
                    Comment because its created at date is older than 2 months ago.
      YAML

      stub_post_mr_from_3_months_ago = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{mr_from_3_months_ago[:iid]}/notes",
        body: { body: 'Comment because its created at date is older than 2 months ago.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_mr_from_3_months_ago)
    end
  end
end
